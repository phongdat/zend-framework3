<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Started;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'started' => [//name route
                'type'    => Segment::class, //route type Segment
                'options' => [
                    'route'    => '/started[/:action][/:id]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                    'constraints' => [
                        'action' => '[a-zA-Z]*',
                        'id'     => '[0-9]*'
                    ]
                ],
            ],
//            'started' => [//name route
//                'type'    => Literal::class, //route type Literal
//                'options' => [
//                    'route'    => '/started/index',
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action'     => 'index',
//                    ],
//                ],
//            ],
//            'started-edit' => [//name route
//                'type'    => Literal::class, //route type Literal
//                'options' => [
//                    'route'    => '/started/edit',
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action'     => 'edit',
//                    ],
//                ],
//            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
